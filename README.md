# dwm - dynamic window manager

My fork of the fantastic [dwm](https://dwm.suckless.org/). Original readme [here](https://git.suckless.org/dwm/file/README.html).

## Patches

-   [alwayscenter](https://dwm.suckless.org/patches/alwayscenter/)
-   [autostart](https://dwm.suckless.org/patches/autostart/)
-   [bottomstack](https://dwm.suckless.org/patches/bottomstack/)
-   [center truncate title](./patches/dwm-centertruncatetitle-6.2.diff)
-   [hide vacant tags](https://dwm.suckless.org/patches/hide_vacant_tags/)
-   [pertag](https://dwm.suckless.org/patches/pertag/) without bar
-   [useless gap](https://dwm.suckless.org/patches/uselessgap/)
-   [warp](https://dwm.suckless.org/patches/warp/)

I didn't use the _alwayscenter_ patch at the time, but my change does the same: center all floating windows.

I only applied the _bstack_ part of _bottomstack_, did not apply _bstackhoriz_. Some manual changes to accommodate gaps.

_center truncate title_ is based on [centeredwindowname](https://dwm.suckless.org/patches/centeredwindowname/). Max title length is 100 chars (as per config).

## Notes

On the old ThinkPad, `mod + shift + right` does not work when left shift is used. Use right shift instead. Not a dwm issue.
